from ReceptionWebInterface import app as application

if __name__ == "__main__":
    application.run()
"""
or

export FLASK_APP=app.py

flask run
"""
