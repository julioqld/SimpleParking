from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.dialects.postgresql import MACADDR, CIDR
from sqlalchemy_utils.types.password import PasswordType
from sqlalchemy_utils.types.choice import ChoiceType
from sqlalchemy.sql import func

from ReceptionWebInterface import db


class User(db.Model):
    """
    User model

    id - user ID
    username - User name
    password - User password
    """
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True, nullable=False)
    password = Column(String, unique=True, nullable=False)
    is_active = Column(Boolean, default=True)
    is_authenticated = Column(Boolean, default=True)
    is_anonymous = Column(Boolean, default=False)
    
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def get_id(self):
        return self.id

    @staticmethod
    def load_user(user_id):
        return User.query.get(user_id)

    def __repr__(self):
        return f"<User (id={self.id}, \
                        username={self.username}, \
                        password={self.password} \
                       ) >"


class Settings(db.Model):
    """
    System settings

    ticket_delete_period - Ticket delete period, in days 
                           (after this period `Temporary` and `ticket_available_to_delete` ticket will be deleted)
    ticket_valid_period - Ticket valid period, in hours
    ticket_text - Text on paper ticket
    """
    __tablename__ = 'settings'

    id = Column(Integer, primary_key=True)
    ticket_delete_period = Column(Integer, default=7)
    ticket_valid_period = Column(Integer, default=7)
    ticket_text = Column(String, default='Simple parking')

    def __init__(self, ticket_delete_period, ticket_valid_period, ticket_text):
        self.ticket_delete_period = ticket_delete_period
        self.ticket_valid_period = ticket_valid_period
        self.ticket_text = ticket_text

    def setting_json(self):
        """
        Method return settings param formated as JSON
        """
        return {
            'ticket_delete_period': self.ticket_delete_period,
            'ticket_valid_period': self.ticket_valid_period,
            'ticket_text': self.ticket_text
        }

    def __repr__(self):
        return f"<Settings (ticket_delete_period={self.ticket_delete_period}, \
                            ticket_valid_period={self.ticket_valid_period}, \
                            ticket_text={self.ticket_text} \
                           ) >"


class Ticket(db.Model):
    """
    Ticket model

    id - Ticket global ID
    ticket_number - Ticket number placed on paper
    ticket_type - Ticket type - `temporary` or `permanent`
    ticket_paid - Ticket paid - `paid` or `unpaid`
    ticket_creation_datetime - Ticket creation datetime
    ticket_valid_datetime - Ticket validation end datetime
    ticket_scanned_datetime - Datetime when ticket is scanned
    ticket_available_to_delete - If tciket is scanned and it is `Temporary` it is available to delete
    visible - Ticket visibility for user(when delete - change to False)
    """
    __tablename__ = 'ticket'
    dict_types = {
                    'temp':'Temporary',
                    'perm':'Permanent',   
                    'unpaid':'Unpaid',   
                    'paid':'Paid',                    
                 }
    temp_ticket = 'temp'
    perm_ticket = 'perm'
    TYPES = [
        (temp_ticket, 'Temporary'),
        (perm_ticket, 'Permanent')
    ]   

    unpaid_ticket = 'unpaid'
    paid_ticket = 'paid' 
    PAID = [
        (unpaid_ticket, 'Unpaid'),
        (paid_ticket, 'Paid')
    ]

    id = Column(Integer, primary_key=True)
    ticket_number = Column(Integer, nullable=False, unique=True)
    ticket_type = Column(ChoiceType(TYPES, impl=String()), default=temp_ticket)
    ticket_paid = Column(ChoiceType(PAID, impl=String()), default=unpaid_ticket)
    ticket_creation_datetime = Column(DateTime(timezone=False), default = datetime.now)
    ticket_valid_datetime = Column(DateTime(timezone=False))
    ticket_scanned_datetime = Column(DateTime(timezone=False), nullable=True)
    ticket_available_to_delete = Column(Boolean, default=False)
    visible = Column(Boolean, default=True)
        
    def __init__(self, ticket_number, ticket_type, ticket_paid, ticket_valid_datetime, visible=True):
        self.ticket_number = ticket_number
        self.ticket_type = ticket_type
        self.ticket_paid = ticket_paid
        self.ticket_valid_datetime = ticket_valid_datetime
        self.visible = visible

    @staticmethod
    def last_ticket():
        """
        Method return last ticket by ID
        """
        return Ticket.query.order_by(Ticket.id.desc()).first()

    def creation_timestamp(self):
        """
        Method return ticket creation datetime in timestamp format
        """
        return self.ticket_creation_datetime.strftime("%s")

    def ticket_json(self):
        """
        Method return ticket data formated as JSON
        """
        return {
            'ticket_number': self.ticket_number,
            'ticket_type': self.ticket_type.code,
            'ticket_paid': self.ticket_paid.code,
            'ticket_creation_datetime': self.ticket_creation_datetime.isoformat(),
            'ticket_creation_timestamp': self.creation_timestamp(),
            'ticket_valid_datetime': self.ticket_valid_datetime.isoformat(),
            'ticket_scanned_datetime': self.ticket_scanned_datetime,
            'ticket_available_to_delete': self.ticket_available_to_delete,
            'visible': self.visible,
        }

    def __repr__(self):
        return f"<Ticket (id={self.id}, \
                          ticket_number={self.ticket_number}, \
                          ticket_type={self.ticket_type}, \
                          creation_datetime={self.ticket_creation_datetime}, \
                          valid_datetime={self.ticket_valid_datetime} \
                         ) >"


class Task(db.Model):
    """
    Task model

    id - Task unique ID
    task_type - Task type
    task_name - Task name
    task_description - Task description
    task_is_complete - Task is cimpleted
    """

    __tablename__ = 'task'
    
    # Automatic actions
    synchro_task = 'synch'
    update_task = 'update'
    # User manually ticket actions
    print_task = 'print'
    add_task = 'add'
    delete_task = 'delete'
    edit_task = 'edit'
    # Gate actions
    open_task = 'open'
    close_task = 'close'
    lock_open_task = 'lock_open'
    lock_close_task = 'lock_close'
    unlock_task = 'unlock'
    # Other actions
    settings_update = 'settings'
    controllers_find = 'controllers_find'
    controllers_check = 'controllers_check'

    TYPES = [
        (synchro_task, 'Synchronize DB'),
        (update_task, 'Update ticket status'),

        (print_task, 'Print ticket'),
        (add_task, 'Add new ticket'),
        (delete_task, 'Delete ticket'),
        (edit_task, 'Edit ticket'),

        (open_task, 'Open'),
        (close_task, 'Close'),
        (lock_open_task, 'Lock open'),
        (lock_close_task, 'Lock close'),
        (unlock_task, 'Unlock'),

        (settings_update, 'Update settings'),
        (controllers_find, 'New controllers find'),
        (controllers_check, 'Exist controllers check'),
    ]

    dict_types = {
                    synchro_task:'Synchronize DB',
                    update_task:'Update ticket status',
                    
                    print_task: 'Print ticket',
                    add_task:'Add new ticket',
                    delete_task:'Delete ticket',  
                    edit_task:'Edit ticket',
                    
                    lock_open_task:'Lock open',
                    lock_close_task:'Lock close',
                    unlock_task:'Unlock',
                    open_task: 'Open',
                    close_task: 'Close',

                    settings_update: 'Update settings',
                    controllers_find: 'New controllers find',
                    controllers_check: 'Exist controllers check',
                 }

    id = Column(Integer, primary_key=True)
    task_type = Column(ChoiceType(TYPES, impl=String()))
    task_name = Column(String(100), index=True)
    task_description = Column(String(500))
    task_start_datetime = Column(DateTime(timezone=False), default = datetime.now)
    task_end_datetime = Column(DateTime(timezone=False))
    task_is_complete = Column(Boolean, default=False)


    def __init__(self, task_name, task_type, task_description):
        self.task_name = task_name
        self.task_type = task_type
        self.task_description = task_description

    def __repr__(self):
        return f"<Task (id={self.id}, \
                        task_name={self.task_name}, \
                        task_description={self.task_description[:20]}, \
                        task_is_complete={self.task_is_complete} \
                       ) >"


class Controller(db.Model):
    """
    Controller model
    All info about all controllers

    id - Controller global ID
    controller_host - Controller IP
    controller_macaddr - Controller MAC address
    controller_type - Controller type
    controller_condition_status - Controller condition status
    controller_position_status - Controller position status
    """
    __tablename__ = 'controller'
    # if controller is not a gate
    not_gate = 'not'
    # For controllers with gates, lock status
    locked_gate = 'lock'
    unlocked_gate = 'unlock'
    lock_opened_gate = 'lock_opened'
    lock_closed_gate = 'lock_closed'
    # For controllers with gates, open/close
    open_gate = 'open'
    close_gate = 'close'
    # Controllers types
    reception_controller = 'reception'
    entry_controller = 'entry'
    exit_controller = 'exit'

    STATUS_CONDITION_TYPES = [
        (not_gate, 'No gate'),
        (locked_gate, 'Locked'),
        (unlocked_gate, 'Unlocked')
    ]

    GATE_POSITION_TYPES = [
        (not_gate, 'No gate'),
        (lock_closed_gate, 'Locked closed'),
        (lock_opened_gate, 'Locked opened'),
    ]

    CONTROLLER_TYPES = [
        (reception_controller, 'Reception'),
        (entry_controller, 'Entry controller'),
        (exit_controller, 'Exit controller')
    ]
    dict_types = {
                    not_gate: 'No gate',
                    open_gate: 'Opened gate',
                    close_gate: 'Closed gate',
                    locked_gate: 'Locked',
                    unlocked_gate: 'Unlocked',  
                    reception_controller: 'Reception',
                    entry_controller: 'Entry controller',
                    exit_controller: 'Exit controller',    
                    lock_closed_gate: 'Locked closed',
                    lock_opened_gate: 'Locked opened',
                 }

    id = Column(Integer, primary_key=True)
    controller_name = Column(String(100), index=True)
    controller_host = Column(CIDR)
    controller_macaddr = Column(MACADDR)
    controller_type = Column(ChoiceType(CONTROLLER_TYPES, impl=String()))
    controller_condition_status = Column(ChoiceType(STATUS_CONDITION_TYPES, impl=String()), default=not_gate)
    controller_position_status = Column(ChoiceType(GATE_POSITION_TYPES, impl=String()), default=not_gate)

    
    def __init__(self, controller_host, controller_macaddr, controller_type, controller_name=None, controller_condition_status=None, controller_position_status=None):
        self.controller_host = controller_host
        self.controller_macaddr = controller_macaddr
        self.controller_type = controller_type
        self.controller_name = controller_name if controller_name else ''
        self.controller_condition_status = controller_condition_status if controller_condition_status else self.not_gate
        self.controller_position_status = controller_position_status if controller_position_status else self.not_gate

    def __repr__(self):
        return f"<Task (id={self.id}, \
                        controller_host={self.controller_host}, \
                        controller_type={self.controller_type.value}, \
                        controller_condition_status={self.controller_condition_status.value}, \
                        controller_position_status={self.controller_position_status.value} \
                       ) >"
