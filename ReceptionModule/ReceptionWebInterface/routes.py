import os
from datetime import datetime, timedelta
from passlib.hash import pbkdf2_sha256

import sqlalchemy_utils
import flask_sqlalchemy
from flask import Flask, render_template, redirect, request, flash, session, url_for
from flask_login import login_required, login_user, logout_user
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import String, cast

from ReceptionWebInterface.forms import LoginForm, NewTicketForm, EditTicketForm, TicketSearchForm, SettingsForm
from ReceptionWebInterface.models import User, Ticket, Settings, Task, Controller
from ReceptionWebInterface import login_manager, app, db
from ReceptionWebInterface.tasks import ticket_delete_task, ticket_add_task, ticket_edit_task, settings_update_task, \
                                  gate_lock_closed_task, gate_lock_opened_task, gate_unlock_task, ticket_print_task, gate_open_task

@login_manager.user_loader
def load_user(user_id):
    return User.load_user(user_id)


@app.route('/')
@app.route('/index/')
def index():
    payload = {'doc': 'pages/index.html',
               'header': 'elements/header.html'}

    return render_template('base.html', payload = payload)


@app.route('/login/', methods = ["GET", "POST"])
def login():
    payload = {'doc': 'pages/login.html',
               'header': 'elements/header.html',
               }

    login_form = LoginForm

    if request.method == 'POST':

        if login_form(request.form).validate():
            # get following user from DB
            result = User.query.filter_by(username=request.form.get('username')).first()

            # if find user
            if result:
                # if valid password
                if pbkdf2_sha256.verify(request.form.get('password'), result.password):
                    flash(f'Success login, {request.form.get("username")}!')
                    session['logged_in'] = True
                    session['username'] = request.form.get('username')
                    # login user
                    login_user(result)
                    
                    return redirect(url_for("index"))
                else:
                    flash('Wrong password')
                    return redirect(url_for("login"))
            else:
                flash('Not valid username data!')
                return redirect(url_for("login"))
                
        else:
            flash('Not valid form!')
            return redirect(url_for("login"))
    else:
        return render_template('base.html', payload = payload, form=login_form())


@app.route('/tickets-list', methods = ["GET", "POST"])
@login_required
def tickets_list():
    payload = {'doc': 'pages/tickets_list.html',
               'header': 'elements/header.html'}

    # rows tickets limit
    ROWS_LIMIT = 20

    # get `order_type`
    order_type=request.args.get('order_type')

    # ticket search form
    search_form = TicketSearchForm

    # get available tickets
    available_tickets = Ticket.query.filter_by(visible=True)

    # if POST request
    if request.method == 'POST':
        # if search butoon clicked
        if search_form(request.form).validate():
            tickets = None
            # TODO finish filtering
            search_form_data = search_form(request.form)

            if search_form_data.ticket_number.data:
                # search ticket by number, using the iLIKE and %<number>% format
                tickets = db.session.query(Ticket).filter(cast(Ticket.ticket_number, String).ilike(f"%{search_form_data.ticket_number.data}%"))

            # search by ticket type - `temp` or `perm`
            if search_form_data.ticket_type.data:
                if tickets:
                    tickets = tickets.filter_by(ticket_type=search_form_data.ticket_type.data)
                else:
                    tickets = Ticket.query.filter_by(ticket_type=search_form_data.ticket_type.data)
                
            # filter by minimal ticket creation datetime
            if search_form_data.ticket_creation_datetime_start.data:
                if tickets:
                    tickets = tickets.filter(Ticket.ticket_creation_datetime>=search_form_data.ticket_creation_datetime_start.data)
                else:
                    tickets = Ticket.query.filter(Ticket.ticket_creation_datetime>=search_form_data.ticket_creation_datetime_start.data)
            
            # filter by maximum ticket creation datetime
            if search_form_data.ticket_creation_datetime_end.data:
                if tickets:
                    tickets = tickets.filter(Ticket.ticket_creation_datetime<=search_form_data.ticket_creation_datetime_end.data)
                else:
                    tickets = Ticket.query.filter(Ticket.ticket_creation_datetime<=search_form_data.ticket_creation_datetime_end.data)
            
            # filter by minimal ticket valid datetime
            if search_form_data.ticket_valid_datetime_start.data:
                if tickets:
                    tickets = tickets.filter(Ticket.ticket_valid_datetime>=search_form_data.ticket_valid_datetime_start.data)
                else:
                    tickets = Ticket.query.filter(Ticket.ticket_valid_datetime>=search_form_data.ticket_valid_datetime_start.data)
            
            # filter by maximum ticket valid datetime
            if search_form_data.ticket_valid_datetime_end.data:
                if tickets:
                    tickets = tickets.filter(Ticket.ticket_valid_datetime<=search_form_data.ticket_valid_datetime_end.data)
                else:
                    tickets = Ticket.query.filter(Ticket.ticket_valid_datetime<=search_form_data.ticket_valid_datetime_end.data)
            
            # filter by minimal ticket scanned datetime
            if search_form_data.ticket_scanned_datetime_start.data:
                if tickets:
                    tickets = tickets.filter(Ticket.ticket_scanned_datetime>=search_form_data.ticket_scanned_datetime_start.data)
                else:
                    tickets = Ticket.query.filter(Ticket.ticket_scanned_datetime>=search_form_data.ticket_scanned_datetime_start.data)
            
            # filter by maximum ticket scanned datetime
            if search_form_data.ticket_scanned_datetime_end.data:
                if tickets:
                    tickets = tickets.filter(Ticket.ticket_scanned_datetime<=search_form_data.ticket_scanned_datetime_end.data)
                else:
                    tickets = Ticket.query.filter(Ticket.ticket_scanned_datetime<=search_form_data.ticket_scanned_datetime_end.data)
            
            # filter by boolean value of `ticket_available_to_delete`
            if search_form_data.ticket_available_to_delete.data:
                if tickets:
                    tickets = tickets.filter_by(ticket_available_to_delete=search_form_data.ticket_available_to_delete.data)
                else:
                    tickets = Ticket.query.filter_by(ticket_available_to_delete=search_form_data.ticket_available_to_delete.data)
            
            if type(tickets) == flask_sqlalchemy.BaseQuery:
                tickets = tickets.filter_by(visible=True).all()
            else:
                tickets = available_tickets.filter_by(visible=True).order_by(Ticket.id.desc()).all()

        # if no search request
        else:
            tickets = available_tickets.filter_by(visible=True).order_by(Ticket.id.desc()).limit(ROWS_LIMIT).all()
        
    else:
        # if order type is selected
        if order_type:
            # order by `order_type`
            if order_type=='id':
                # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
                if session.get('tickets_ordering') == 'id' and session.get('tickets_ordering_type') == 'desc':
                    tickets = available_tickets.order_by(Ticket.id)
                    session['tickets_ordering_type']='straight'
                else:    
                    tickets = available_tickets.order_by(Ticket.id.desc())
                    session['tickets_ordering_type']='desc'
            
            elif order_type=='number':
                # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
                if session.get('tickets_ordering') == 'number' and session.get('tickets_ordering_type') == 'desc':
                    tickets = available_tickets.order_by(Ticket.ticket_number)
                    session['tickets_ordering_type']='straight'
                else:    
                    tickets = available_tickets.order_by(Ticket.ticket_number.desc())
                    session['tickets_ordering_type']='desc'
            
            elif order_type=='type':
                # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
                if session.get('tickets_ordering') == 'type' and session.get('tickets_ordering_type') == 'desc':
                    tickets = available_tickets.order_by(Ticket.ticket_type)
                    session['tickets_ordering_type']='straight'
                else:    
                    tickets = available_tickets.order_by(Ticket.ticket_type.desc())
                    session['tickets_ordering_type']='desc'
            
            elif order_type=='creation_datetime':
                # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
                if session.get('tickets_ordering') == 'creation_datetime' and session.get('tickets_ordering_type') == 'desc':
                    tickets = available_tickets.order_by(Ticket.ticket_creation_datetime)
                    session['tickets_ordering_type']='straight'
                else:    
                    tickets = available_tickets.order_by(Ticket.ticket_creation_datetime.desc())
                    session['tickets_ordering_type']='desc'

            elif order_type=='valid_datetime':
                # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
                if session.get('tickets_ordering') == 'valid_datetime' and session.get('tickets_ordering_type') == 'desc':
                    tickets = available_tickets.order_by(Ticket.ticket_valid_datetime)
                    session['tickets_ordering_type']='straight'
                else:    
                    tickets = available_tickets.order_by(Ticket.ticket_valid_datetime.desc())
                    session['tickets_ordering_type']='desc'

            elif order_type=='scanned_datetime':
                # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
                if session.get('tickets_ordering') == 'scanned_datetime' and session.get('tickets_ordering_type') == 'desc':
                    tickets = available_tickets.order_by(Ticket.ticket_scanned_datetime)
                    session['tickets_ordering_type']='straight'
                else:    
                    tickets = available_tickets.order_by(Ticket.ticket_scanned_datetime.desc())
                    session['tickets_ordering_type']='desc'

            elif order_type=='to_delete':
                # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
                if session.get('tickets_ordering') == 'to_delete' and session.get('tickets_ordering_type') == 'desc':
                    tickets = available_tickets.order_by(Ticket.ticket_available_to_delete)
                    session['tickets_ordering_type']='straight'
                else:    
                    tickets = available_tickets.order_by(Ticket.ticket_available_to_delete.desc())
                    session['tickets_ordering_type']='desc'
            else:
                tickets = available_tickets.order_by(Ticket.ticket_number.desc())

            # rewrite `tickets_ordering` in session
            session['tickets_ordering']=order_type

            tickets = tickets.limit(ROWS_LIMIT).all()

        # if no ordering set
        else:
            tickets = available_tickets.order_by(Ticket.id.desc()).limit(ROWS_LIMIT).all()
            
    # TODO add tickets limit and pagination
    payload['tickets']=tickets

    return render_template('base.html', payload = payload, form = search_form())


@app.route('/tasks-list')
@login_required
def tasks_list():
    payload = {'doc': 'pages/tasks_list.html',
               'header': 'elements/header.html'}
    # rows tasks limit
    ROWS_LIMIT = 20

    # get `order_type`
    order_type=request.args.get('order_type')

    # if order type is selected
    if order_type:
        # get available tasks
        all_tasks = Task.query
        # order by `order_type`
        if order_type=='id':
            # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
            if session.get('tasks_ordering') == 'id' and session.get('tasks_ordering_type') == 'desc':
                all_tasks = all_tasks.order_by(Task.id)
                session['tasks_ordering_type']='straight'
            else:    
                all_tasks = all_tasks.order_by(Task.id.desc())
                session['tasks_ordering_type']='desc'
        
        elif order_type=='name':
            # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
            if session.get('tasks_ordering') == 'name' and session.get('tasks_ordering_type') == 'desc':
                all_tasks = all_tasks.order_by(Task.task_name)
                session['tasks_ordering_type']='straight'
            else:    
                all_tasks = all_tasks.order_by(Task.task_name.desc())
                session['tasks_ordering_type']='desc'
        
        elif order_type=='type':
            # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
            if session.get('tasks_ordering') == 'type' and session.get('tasks_ordering_type') == 'desc':
                all_tasks = all_tasks.order_by(Task.task_type)
                session['tasks_ordering_type']='straight'
            else:    
                all_tasks = all_tasks.order_by(Task.task_type.desc())
                session['tasks_ordering_type']='desc'
        
        elif order_type=='start_datetime':
            # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
            if session.get('tasks_ordering') == 'start_datetime' and session.get('tasks_ordering_type') == 'desc':
                all_tasks = all_tasks.order_by(Task.task_start_datetime)
                session['tasks_ordering_type']='straight'
            else:    
                all_tasks = all_tasks.order_by(Task.task_start_datetime.desc())
                session['tasks_ordering_type']='desc'

        elif order_type=='end_datetime':
            # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
            if session.get('tasks_ordering') == 'end_datetime' and session.get('tasks_ordering_type') == 'desc':
                all_tasks = all_tasks.order_by(Task.task_end_datetime)
                session['tasks_ordering_type']='straight'
            else:    
                all_tasks = all_tasks.order_by(Task.task_end_datetime.desc())
                session['tasks_ordering_type']='desc'

        elif order_type=='completed':
            # if this `order_type` already in use and `order_by.desc()`, order without `desc()`
            if session.get('tasks_ordering') == 'completed' and session.get('tasks_ordering_type') == 'desc':
                all_tasks = all_tasks.order_by(Task.task_is_complete)
                session['tasks_ordering_type']='straight'
            else:    
                all_tasks = all_tasks.order_by(Task.task_is_complete.desc())
                session['tasks_ordering_type']='desc'

        else:
            all_tasks = all_tasks.order_by(Task.id.desc())

        # rewrite `tasks_ordering` in session
        session['tasks_ordering']=order_type

        # limit results
        all_tasks = all_tasks.limit(ROWS_LIMIT)

    # TODO add filters on template
    # if no ordering set
    else:
        all_tasks = Task.query.order_by(Task.id.desc()).limit(ROWS_LIMIT).all()
        
    # TODO add tasks limit and pagination
    payload['tasks'] = all_tasks

    return render_template('base.html', payload = payload)


@app.route('/new-ticket/', methods = ["GET", "POST"])
@login_required
def new_ticket():
    payload = {'doc': 'pages/new_ticket.html',
               'header': 'elements/header.html'}

    new_ticket_form = NewTicketForm

    # get ticket_valid_period data
    ticket_valid_period_data = Settings.query.first().ticket_valid_period

    # prepare new default ticket_valid_datetime to form
    # get now date + ticket_valid_period
    prepared_ticket_valid = datetime.now()+timedelta(hours=ticket_valid_period_data)

    if request.method == 'POST':

        if new_ticket_form(request.form).validate():
            # check if ticket already exist
            ticket = Ticket.query.filter_by(ticket_number=request.form.get('ticket_number')).first()

            if not ticket:
                # format ticket datetime to valid format
                ticket_formated_datetime = datetime.strptime(request.form.get('ticket_valid_datetime'), '%Y-%m-%dT%H:%M')
                
                # create new ticket object
                new_ticket_obj = Ticket(ticket_number=request.form.get('ticket_number'), 
                                        ticket_type=request.form.get('ticket_type'), 
                                        ticket_paid=request.form.get('ticket_pay'),
                                        ticket_valid_datetime=ticket_formated_datetime
                                       )
                db.session.add(new_ticket_obj)
                db.session.commit()
                # get created ticket ID
                db.session.flush()
                # send new ticket to other DB's
                ticket_add_task.delay(new_ticket_obj.id)

                flash(f'Ticket #{request.form.get("ticket_number")} success created!')
            else:
                flash(f'Error while ticket #{request.form.get("ticket_number")} creation!\n Ticket already exist.')
        else:
            flash(f'Error while tciket creation!\n Check fileds valid.')
        
        return redirect(url_for('new_ticket'))

    return render_template('base.html', payload = payload, form=new_ticket_form(ticket_number = Ticket.last_ticket().ticket_number+1,
                                                                                ticket_valid_datetime=prepared_ticket_valid))


@app.route('/edit-ticket/<int:ticket_id>', methods = ["GET", "POST"])
@login_required
def edit_ticket(ticket_id):
    payload = {'doc': 'pages/edit_ticket.html',
               'header': 'elements/header.html'}

    edit_ticket_form = EditTicketForm
    # get editable ticket
    used_ticket = Ticket.query.filter_by(id=ticket_id, visible=True).first()
    # add ticket to payload
    payload.update({'ticket': used_ticket})

    if request.method == 'POST':

        if edit_ticket_form(request.form).validate():
            # format ticket datetime to valid format
            ticket_formated_datetime = datetime.strptime(request.form.get('ticket_valid_datetime'), '%Y-%m-%dT%H:%M')
                        
            # edit exist ticket object
            used_ticket.ticket_type=sqlalchemy_utils.types.choice.Choice(code=request.form.get('ticket_type'), 
                                                                         value=Ticket.dict_types[request.form.get('ticket_type')])
            used_ticket.ticket_valid_datetime=ticket_formated_datetime

            # commit new ticket data to db   
            db.session.commit()

            # send new ticket to other DB's
            ticket_edit_task.delay(used_ticket.id)
            
            flash(f'Ticket #{ticket_id} success edited!')

            # redirect back to exist page
            return redirect(url_for('edit_ticket', ticket_id=ticket_id))
        else:
            flash(f'Error while tciket creation!\n Check fileds valid.')

    # set editable ticket form init values and render it
    return render_template('base.html', payload = payload, form=edit_ticket_form(ticket_type=used_ticket.ticket_type.code ,
                                                                                 ticket_valid_datetime=used_ticket.ticket_valid_datetime))


@app.route('/delete-ticket/<int:ticket_id>')
@login_required
def delete_ticket(ticket_id):
    # init new remote task
    ticket_delete_task.delay(ticket_id = ticket_id)

    # get deleted ticket
    deleted_ticket = Ticket.query.get(ticket_id)
    deleted_ticket.visible = False
    # commit new ticket data to db
    db.session.commit()

    flash(f'Ticket #{ticket_id} success deleted!')

    return redirect(url_for('tickets_list'))


@app.route('/print-ticket/<int:ticket_id>')
@login_required
def print_ticket(ticket_id):
    # init new remote task
    ticket_print_task.delay(ticket_id = ticket_id)

    flash(f'Ticket #{ticket_id} will be printed on local printer!')

    return redirect(url_for('tickets_list'))



@app.route('/settings/', methods = ["GET", "POST"])
@login_required
def settings():
    payload = {'doc': 'pages/settings.html',
               'header': 'elements/header.html'}

    settings_form = SettingsForm
    settings_data = Settings.query.first()

    if request.method == 'POST':

        if settings_form(request.form).validate():
            # set data to form
            settings_form_data = settings_form(request.form)

            # upgrade settings data
            settings_data.ticket_delete_period = settings_form_data.ticket_delete_period.data
            settings_data.ticket_valid_period = settings_form_data.ticket_valid_period.data
            settings_data.ticket_text = settings_form_data.ticket_text.data

            # commit new settings data to db   
            db.session.commit()

            # update settings data on Exit module
            settings_update_task.delay()

            flash(f'Settings data updated!')

            return redirect(url_for("settings"))

        else:
            flash(f'Form not valid, recheck it!')

    return render_template('base.html', payload = payload, form=settings_form(ticket_delete_period=settings_data.ticket_delete_period,
                                                                              ticket_text=settings_data.ticket_text,
                                                                              ticket_valid_period=settings_data.ticket_valid_period))


@app.route('/control-page/', methods = ["GET", "POST"])
@login_required
def control_page():
    payload = {'doc': 'pages/control_page.html',
               'header': 'elements/header.html'}

    controllers_data = Controller.query.order_by(Controller.id)

    filter_by_type = request.args.get('filter_by_type')
    if filter_by_type:
        controllers_data = controllers_data.filter(Controller.controller_type.in_((filter_by_type, Controller.reception_controller))).all()
    
    if request.method == 'POST':
        # if user click lock the gate
        if request.form.get('open_onetime_button'):
            # get clicked controller and update status
            controller = Controller.query.get(request.form.get('open_onetime_button'))

            gate_open_task.delay(controller_id=controller.id)

            flash(f'{controller.controller_name} - opened one time!')
        
        # if user click to unlock the gate
        elif request.form.get('unlock_button'):
            # get clicked controller and update status
            updated_controller = Controller.query.get(request.form.get('unlock_button'))
            updated_controller.controller_condition_status = sqlalchemy_utils.types.choice.Choice(code=Controller.unlocked_gate, 
                                                                                                  value=Controller.dict_types[Controller.unlocked_gate])
            updated_controller.controller_position_status = sqlalchemy_utils.types.choice.Choice(code=Controller.not_gate, 
                                                                                                 value=Controller.dict_types[Controller.not_gate])
            db.session.commit()

            gate_unlock_task.delay(controller_id=request.form.get('unlock_button'))

            flash(f'{updated_controller.controller_name} - unlocked!')
        
        # if user click to open gate button
        elif request.form.get('lock_closed_button'):
            # get clicked controller and update status
            updated_controller = Controller.query.get(request.form.get('lock_closed_button'))
            updated_controller.controller_condition_status = sqlalchemy_utils.types.choice.Choice(code=Controller.locked_gate, 
                                                                                                  value=Controller.dict_types[Controller.locked_gate])

            updated_controller.controller_position_status = sqlalchemy_utils.types.choice.Choice(code=Controller.lock_closed_gate, 
                                                                                                  value=Controller.dict_types[Controller.lock_closed_gate])
            db.session.commit()

            gate_lock_closed_task.delay(controller_id=request.form.get('lock_closed_button'))

            flash(f'{updated_controller.controller_name} - lock closed!')
        
        # if user click to close gate button
        elif request.form.get('lock_opened_button'):
            # get clicked controller and update status
            updated_controller = Controller.query.get(request.form.get('lock_opened_button'))
            updated_controller.controller_condition_status = sqlalchemy_utils.types.choice.Choice(code=Controller.locked_gate, 
                                                                                                  value=Controller.dict_types[Controller.locked_gate])
            
            updated_controller.controller_position_status = sqlalchemy_utils.types.choice.Choice(code=Controller.lock_opened_gate, 
                                                                                                  value=Controller.dict_types[Controller.lock_opened_gate])
            
            db.session.commit()

            gate_lock_opened_task.delay(controller_id=request.form.get('lock_opened_button'))

            flash(f'{updated_controller.controller_name} - lock opened!')
        
    payload['controlers'] = controllers_data

    return render_template('base.html', payload = payload)


@app.route('/logout/')
@login_required
def logout():
    logout_user()
    session.clear()
    flash('Success logout.')
    return redirect(url_for("index"))
