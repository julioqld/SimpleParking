import json

import pika

class ReceptionQueueClient:

    def __init__(self, rtmq_username: str = 'guest', rtmq_password: str = 'guest', rtmq_host: str = 'localhost', rtmq_port: int = '5672', rtmq_vhost: str = None):
        self.rtmq_username = rtmq_username
        self.rtmq_password = rtmq_password
        self.rtmq_host = rtmq_host
        self.rtmq_port = rtmq_port
        self.rtmq_vhost = rtmq_vhost
        
        # connect to queue
        parameters = pika.URLParameters(f'amqp://{self.rtmq_username}:{self.rtmq_password}@{self.rtmq_host}:{self.rtmq_port}{"/"+self.rtmq_vhost if self.rtmq_vhost else ""}')
        self.connection = pika.BlockingConnection(parameters=parameters)
        self.channel = self.connection.channel()

    def get_message(self, queue_name: str):
        # create queue
        self.__create_queue(queue_name=queue_name)
        
        method_frame, _, body = self.channel.basic_get(queue_name)
        if body and method_frame:
            self.__success_deliver(method_frame)
            return body.decode()
        
        return False
            
    def create_message(self, message_body: str, queue_name: str):
        # create queue
        self.__create_queue(queue_name=queue_name)

        # send message to queue
        self.channel.basic_publish(exchange='',
                                   routing_key=queue_name,
                                   body=message_body)
        print(f" [x] Sent '{message_body}'")

    def __success_deliver(self, method_frame: pika.spec.Basic.GetOk):
        # if message success readed
        self.channel.basic_ack(delivery_tag = method_frame.delivery_tag)

    def __create_queue(self, queue_name: str):
        # create queue
        self.channel.queue_declare(queue=queue_name, durable=True)

    def __del__(self):
        # close connection
        self.connection.close()
        