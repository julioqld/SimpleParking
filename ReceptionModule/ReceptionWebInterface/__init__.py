from datetime import datetime

from flask import Flask
from celery import Celery
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

from ReceptionWebInterface.config import Config
from .QueueClient import ReceptionQueueClient

app = Flask(__name__)
app.config.from_object(Config)

# login
login_manager = LoginManager(app)
login_manager.login_view = 'login'

# DB
db = SQLAlchemy(app)
migrate = Migrate(app, db)

@app.before_first_request
def insert_initial_values():
    """
    Insert first values to DB
    - Add new controllers in network
    - User login data
    - Settings default
    - Ticket initial ticket
    """
    from ReceptionWebInterface.models import User, Settings, Ticket
    from ReceptionWebInterface.tasks import new_controller_search, update_exist_controllers

    # check new `controllers` in NET and insert in DB
    new_controller_search.delay()
    # update exist controllers
    update_exist_controllers.delay()

    # add new user
    if not User.query.all():
        db.session.add(User(username='AndreiDrang', 
                            password="$pbkdf2-sha256$29000$sxZCSCklpDQGIGQMIeQcIw$Tfdfc0TF6mK.xZBIzXHNjy2u0IZ1kQI1V9uEVifF6AQ"))
    # add basic setting
    if not Settings.query.all():
        db.session.add(Settings(ticket_delete_period=7,
                                ticket_valid_period=3,
                                ticket_text='Hello in Simple Parking'))
    # add first ticket
    if not Ticket.query.all():
        db.session.add(Ticket(ticket_number=1000,
                              ticket_type=Ticket.temp_ticket,
                              ticket_paid=Ticket.unpaid_ticket,
                              ticket_valid_datetime=datetime.now(),
                              visible=False))
    db.session.commit()
    db.session.close()


# Celery
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'rpc://'
app.config['TASK_SERIALIZER'] = 'json'
app.config['ACCEPT_CONTENT'] = 'json'
app.config['RESULT_SERIALIZER'] = 'json'
app.config['C_FORCE_ROOT'] = True

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

from ReceptionWebInterface import models, routes
