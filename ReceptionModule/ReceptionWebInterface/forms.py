from datetime import datetime

#from wtforms import Form
from wtforms import StringField, PasswordField, validators, DateTimeField, IntegerField, SelectField, BooleanField
from flask_wtf import FlaskForm as Form

class LoginForm(Form):
    """
    Login form
    username - username of login person
    password - password of login person
    """
    username = StringField('Username', validators=[validators.Length(min=4, max=25),
                                                   validators.DataRequired()])
    password = PasswordField('New Password', validators=[validators.Length(min=4, max=25),
                                                         validators.DataRequired()])


class NewTicketForm(Form):
    """
    New ticket form
    ticket_number - Ticket number placed on paper
    ticket_type - Ticket type - `temporary` or `permanent`
    ticket_pay - Ticket pay status - `paid` or `unpaid`
    ticket_valid_datetime - Ticket validation end datetime
    """
    TYPES = [
        ('perm', 'Permanent'),
        ('temp', 'Temporary')
    ]
    PAID = [
        ('unpaid', 'Unpaid'),
        ('paid', 'Paid')
    ]

    ticket_number = IntegerField('Ticket number', validators=[validators.DataRequired()])

    ticket_type = SelectField('Choice type', choices=TYPES,
                                             validators=[validators.DataRequired()])

    ticket_pay = SelectField('Choice pay status', choices=PAID,
                                                  validators=[validators.DataRequired()])

    ticket_valid_datetime = DateTimeField('Valid date-time', validators=[validators.DataRequired()],
                                           format = "%Y-%m-%dT%H:%M",default= datetime.now)


class TicketSearchForm(Form):
    """
    Ticket search form

    ticket_number - Ticket number
    ticket_type - Ticket type
    ticket_creation_datetime_start - Ticket creation datetime start
    ticket_creation_datetime_end - Ticket creation datetime end
    ticket_valid_datetime_start - Ticket validation end datetime start
    ticket_valid_datetime_end - Ticket validation end datetime end
    ticket_scanned_datetime_start - Ticket scann datetime start
    ticket_scanned_datetime_end - Ticket scann datetime end
    ticket_available_to_delete - Ticket available to delete
    """
    TYPES = [
        ('',     '---------'),
        ('temp', 'Temporary'),
        ('perm', 'Permanent')
    ]

    ticket_number = IntegerField('Ticket number', validators=[validators.Optional()])

    ticket_type = SelectField('Choice type', choices=TYPES, default = '', validators=[validators.Optional()])

    ticket_creation_datetime_start = DateTimeField('Creation date-time start/end', validators=[validators.Optional()],
                                                   format = "%Y-%m-%dT%H:%M")

    ticket_creation_datetime_end = DateTimeField('Creation date-time end', validators=[validators.Optional()],
                                                 format = "%Y-%m-%dT%H:%M")

    ticket_valid_datetime_start = DateTimeField('Valid date-time start/end', validators=[validators.Optional()],
                                                format = "%Y-%m-%dT%H:%M")

    ticket_valid_datetime_end = DateTimeField('Valid date-time end', validators=[validators.Optional()],
                                              format = "%Y-%m-%dT%H:%M")

    ticket_scanned_datetime_start = DateTimeField('Scann date-time start/end', validators=[validators.Optional()],
                                                  format = "%Y-%m-%dT%H:%M")

    ticket_scanned_datetime_end = DateTimeField('Scann date-time end', validators=[validators.Optional()],
                                                format = "%Y-%m-%dT%H:%M")

    ticket_available_to_delete = BooleanField('Available to delete', validators=[validators.Optional()],
                                              default = False)


class EditTicketForm(Form):
    """
    Edit ticket form
    ticket_type - Ticket type - `temporary` or `permanent`
    ticket_valid_datetime - Ticket validation end datetime
    """
    TYPES = [
        ('temp', 'Temporary'),
        ('perm', 'Permanent')
    ]
    ticket_type = SelectField('Choice type', choices=TYPES,
                                             validators=[validators.DataRequired()])

    ticket_valid_datetime = DateTimeField('Valid date-time', validators=[validators.DataRequired()],
                                          format = "%Y-%m-%dT%H:%M",
                                          default = datetime.now)


class SettingsForm(Form):
    """
    Edit settings form
    ticket_delete_period - Ticket delete period, in days
    ticket_valid_period - Ticket valid period, in days
    ticket_text - Text on ticket paper
    """
    ticket_delete_period = IntegerField('Ticket delete period',
                                        validators=[validators.DataRequired()])

    ticket_valid_period = IntegerField('Ticket valid period',
                                       validators=[validators.DataRequired()])

    ticket_text = StringField('Ticket text', 
                              validators=[validators.DataRequired()])
