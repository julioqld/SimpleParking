import time
import random
import json
import urllib
import socket
import subprocess
import re
from datetime import datetime

from celery.task import periodic_task
from celery.schedules import crontab
from scapy.all import srp
from scapy.all import Ether, ARP, conf

from ReceptionWebInterface import celery, db, ReceptionQueueClient
from ReceptionWebInterface.models import Task, Settings, Controller, Ticket

"""
celery -A ReceptionWebInterface.celery worker -B -l info --concurrency=10
"""

# TODO connect to controlers RabbitMQ-queues by controller IP

# base instruction message to queue
INSTRUCTION = {
    'task': '',
    'text': ''
}

def arping(ip_range: str)->list:
    """
    Arping function takes IP Address or Network, returns nested ip list

    :param ip_range: Local net mask, for example - `192.168.*.*`
    """
    # switch off console logs from scapy
    conf.verb = 0
    # list of detected IPs
    ips_list = []
    # get all IP's in local network
    answer, _ = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip_range), timeout=2)
    # looping IP's results
    for _, rcv in answer:
        # inserting IP's in prepared list
        ips_list.append(rcv.sprintf(r"%ARP.psrc%"))

    return(ips_list)

def get_network_masc()->str:
    """
    Function get local machine address, delete last number after point and change it on `*`. 
    For wxample: `192.168.0.111` ---> `192.168.0.*`
    """
    # get local machine IP, split/join -> delete last number and get local network masc.
    machine_ip = '.'.join(subprocess.check_output(['hostname','-I']).decode().replace(" \n", '').split('.')[:3])
    # add to mask `.*` and return it to `arping` function    
    return f'{machine_ip}.*'

def clean_ip_address(ip_address: str)->str:
    """
    Function get IP address as string and delete IP range(.../32) from string

    :param ip_address: String with IP address, for example - `192.168.100.12/32`
    """
    # pre-compile clean ip address reg-exp
    clean_ip_pattern = re.compile('(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})/\d{1,3}')
    # find all right substring in string
    result = re.findall(clean_ip_pattern, ip_address)

    return result[0]


"""
User manually ticket actions
"""

@celery.task
def ticket_print_task(ticket_id: int):
    """
    Connect to LOCAL printer and print ticket
    """
    # get ticket data
    used_ticket = Ticket.query.get(ticket_id)
    # add new task
    new_task = Task(task_name=f'Print ticket #{used_ticket.ticket_number}',
                    task_description='Ticket printed by reception user',
                    task_type=Task.print_task)
    # add new task to db
    db.session.add(new_task)

    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """
    # TODO add ticket printing operation

    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.remove()


@celery.task
def ticket_add_task(ticket_id: int):
    """
    Connect to EXIT remote DB and add new ticket
    """
    # get ticket data
    used_ticket = Ticket.query.get(ticket_id)
    # add new task
    new_task = Task(task_name=f'New ticket #{used_ticket.ticket_number}',
                    task_description='New ticket add to DB. Ticket add by reception user',
                    task_type=Task.add_task)
    # add new task to db
    db.session.add(new_task)

    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """
    # print ticket
    ticket_print_task.delay(ticket_id=ticket_id)

    # create list of connected exit controllers
    exit_controllers_list = Controller.query.filter_by(controller_type=Controller.exit_controller).all()

    # insert instruction to message
    INSTRUCTION.update({'task': Task.add_task,
                        'text': used_ticket.ticket_json()})

    # looping exit controllers
    for controller in exit_controllers_list:
        # connect to controller queue and send message
        try:
            # connect to controller queue and send message
            ReceptionQueueClient(rtmq_username='reception',
                                 rtmq_password='password',
                                 rtmq_host=clean_ip_address(controller.controller_host)
                                ).create_message(message_body=json.dumps(INSTRUCTION), queue_name='specific_gate_queue')
            
        except Exception as err:
            # TODO Add logging
            print(err)

    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.remove()


@celery.task
def ticket_delete_task(ticket_id: int):
    """
    Connect to EXIT remote DB and delete ticket
    """
    # get ticket data
    used_ticket = Ticket.query.get(ticket_id)
    # add new task
    new_task = Task(task_name=f'Delete ticket #{used_ticket.ticket_number}',
                    task_description='Delete ticket from DB. Deletion make by reception user',
                    task_type=Task.delete_task)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """

    # create list of connected exit controllers
    exit_controllers_list = Controller.query.filter_by(controller_type=Controller.exit_controller).all()

    # insert instruction to message
    INSTRUCTION.update({'task': Task.delete_task,
                        'text': used_ticket.ticket_json()})

    # looping exit controllers
    for controller in exit_controllers_list:
        # connect to controller queue and send message
        try:
            # connect to controller queue and send message
            ReceptionQueueClient(rtmq_username='reception',
                                 rtmq_password='password',
                                 rtmq_host=clean_ip_address(controller.controller_host)
                                ).create_message(message_body=json.dumps(INSTRUCTION), queue_name='specific_gate_queue')
            
        except Exception as err:
            # TODO Add logging
            print(err)
    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()


@celery.task
def ticket_edit_task(ticket_id: int):
    """
    Connect to EXIT remote DB and update ticket data
    """
    # get ticket data
    used_ticket = Ticket.query.get(ticket_id)
    # add new task
    new_task = Task(task_name=f'Edit ticket #{used_ticket.ticket_number}',
                    task_description='Ticket edit in DB. Edition make by reception user',
                    task_type=Task.edit_task)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """
    # create list of connected exit controllers
    exit_controllers_list = Controller.query.filter_by(controller_type=Controller.exit_controller).all()

    # insert instruction to message
    INSTRUCTION.update({'task': Task.edit_task,
                        'text': used_ticket.ticket_json()})

    # looping exit controllers
    for controller in exit_controllers_list:
        # connect to controller queue and send message
        try:
            # connect to controller queue and send message
            ReceptionQueueClient(rtmq_username='reception',
                                 rtmq_password='password',
                                 rtmq_host=clean_ip_address(controller.controller_host)
                                ).create_message(message_body=json.dumps(INSTRUCTION), queue_name='specific_gate_queue')
            
        except Exception as err:
            # TODO Add logging
            print(err)

    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()


"""
Gate actions
"""

@celery.task
def gate_open_task(controller_id: int):
    """
    Connect to ENTRY/EXIT remote controller, open gate
    """
    # get controller by ID
    used_controller = Controller.query.get(controller_id)
    # add new task
    new_task = Task(task_name=f'Open - {used_controller.controller_name}',
                    task_description=f'{used_controller.controller_name} - opened by reception user',
                    task_type=Task.open_task)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """
    INSTRUCTION.update({'task': Task.open_task})
    try:
        # connect to controller queue and send message
        ReceptionQueueClient(rtmq_username='reception',
                             rtmq_password='password',
                             rtmq_host=clean_ip_address(used_controller.controller_host)
                            ).create_message(message_body=json.dumps(INSTRUCTION), queue_name='gate_queue')
    
    except Exception as err:
        # TODO Add logging
        print(err) 

    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()


@celery.task
def gate_lock_closed_task(controller_id: int):
    """
    Connect to EXIT/ENTRY remote controller, stop main cycle and LOCK gates
    """
    # get controller by ID
    used_controller = Controller.query.get(controller_id)
    # add new task
    new_task = Task(task_name=f'Lock closed - {used_controller.controller_name}',
                    task_description=f'{used_controller.controller_name} - locked closed by reception user',
                    task_type=Task.lock_close_task)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """
    # insert instruction
    INSTRUCTION.update({'task': Task.lock_close_task})
    try:
        # connect to controller queue and send message
        ReceptionQueueClient(rtmq_username='reception',
                             rtmq_password='password',
                             rtmq_host=clean_ip_address(used_controller.controller_host)
                            ).create_message(message_body=json.dumps(INSTRUCTION), queue_name='gate_queue')
    
    except Exception as err:
        # TODO Add logging
        print(err)
    
    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()


@celery.task
def gate_lock_opened_task(controller_id: int):
    """
    Connect to EXIT/ENTRY remote controller, stop main cycle and LOCK gates
    """
    # get controller by ID
    used_controller = Controller.query.get(controller_id)
    # add new task
    new_task = Task(task_name=f'Lock opened - {used_controller.controller_name}',
                    task_description=f'{used_controller.controller_name} - locked opened by reception user',
                    task_type=Task.lock_open_task)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """
    # insert instruction
    INSTRUCTION.update({'task': Task.lock_open_task})
    try:
        # connect to controller queue and send message
        ReceptionQueueClient(rtmq_username='reception',
                             rtmq_password='password',
                             rtmq_host=clean_ip_address(used_controller.controller_host)
                            ).create_message(message_body=json.dumps(INSTRUCTION), queue_name='gate_queue')
    
    except Exception as err:
        # TODO Add logging
        print(err)    

    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()


@celery.task
def gate_unlock_task(controller_id: int):
    """
    Connect to EXIT/ENTRY remote controller, start main cycle and UNLOCK gates
    """
    # get controller by ID
    used_controller = Controller.query.get(controller_id)
    # add new task
    new_task = Task(task_name=f'UN_Lock - {used_controller.controller_name}',
                    task_description=f'{used_controller.controller_name} - unlocked by reception user',
                    task_type=Task.unlock_task)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """
    # insert instruction
    INSTRUCTION.update({'task': Task.unlock_task})
    # connect to controller queue and send message
    try:
        # connect to controller queue and send message
        ReceptionQueueClient(rtmq_username='reception',
                             rtmq_password='password',
                             rtmq_host=clean_ip_address(used_controller.controller_host)
                            ).create_message(message_body=json.dumps(INSTRUCTION), queue_name='gate_queue')
    
    except Exception as err:
        # TODO Add logging
        print(err)
    
    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()


"""
Other actions
"""

@celery.task
def settings_update_task():
    """
    Connect to EXIT/ENTRY remote DB and update ticket data
    """
    # get controllers by type = `exit`
    used_controllers = Controller.query.filter_by(controller_type=Controller.exit_controller).all()

    # add new task
    new_task = Task(task_name=f'Update settings data',
                    task_description='Updating settings data by reception user.',
                    task_type=Task.settings_update)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    """
    MAIN LOGIC
    """    
    # get settings data
    settings_data = Settings.query.all()[0]

    # insert instruction for message
    INSTRUCTION.update({'task': Task.settings_update,
                        'text': settings_data.setting_json()})

    # looping exit controllers
    for controller in used_controllers:
        # connect to controller queue and send message
        try:
            # connect to controller queue and send message
            ReceptionQueueClient(rtmq_username='reception',
                                 rtmq_password='password',
                                 rtmq_host=clean_ip_address(controller.controller_host)
                                ).create_message(message_body=json.dumps(INSTRUCTION), queue_name='specific_gate_queue')
            
        except Exception as err:
            # TODO Add logging
            print(err)

    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()

#Execute daily at midnight, production - crontab(minute=0, hour=0)	
@periodic_task(run_every=crontab(minute='*/30'))
def new_controller_search():
    """
    Periodic task which check all devices in local NET try find new or update old
    """
    # add new task
    new_task = Task(task_name=f'Find new controllers',
                    task_description='Automatic find new controllers.',
                    task_type=Task.controllers_find)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    # create list of local addresses
    ips = arping(ip_range=get_network_masc())
    print(ips)
    # looping IP addresses list
    for ip in ips:
        try:
            # ping all local addresses
            response_decoded = urllib.request.urlopen(f'http://{ip}:8001/PING', timeout=15).read().decode('utf-8')
            response_json = json.loads(response_decoded)
            # check if response containt `success`=PONG
            if response_json['success']=='PONG':
                # try to get controller by mac_address and type
                controller_by_mac = Controller.query.filter_by(controller_macaddr=response_json['controller_mac'],
                                                               controller_type=response_json['controller_type']).first()
                # if controller not already exist - create
                if not controller_by_mac:
                    # create new controller
                    db.session.add(Controller(controller_host=response_json['controller_ip'],
                                              controller_macaddr=response_json['controller_mac'],
                                              controller_type=response_json['controller_type'],
                                              controller_name=f"Controller {response_json['controller_type']}",
                                              controller_condition_status=Controller.unlocked_gate))

                    print(f'New controller created! Name - Controller {response_json["controller_type"]}')
                # if exist - update IP address
                else: 
                    controller_by_mac.controller_host = response_json['controller_ip']

                    print(f'Already exist controller updated! Name - {controller_by_mac.controller_name}')

                db.session.commit()

        except urllib.error.URLError as err:
            pass
        except Exception as err:
            # TODO Add logging
            print(ip)
            print(err)
            pass

    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()


#Execute daily at midnight, production - crontab(minute=0, hour=0)	
@periodic_task(run_every=crontab(minute='*/20'))
def update_exist_controllers():
    """
    Periodic task which check all exist controllers in DB, try to connect and check it, if fail - delete them from list.
    """
    # add new task
    new_task = Task(task_name=f'Update exist controllers',
                    task_description='Automatic updating exist controllers list.',
                    task_type=Task.controllers_check)
    # add new task to db
    db.session.add(new_task)
    # At this point, the object `new_task` has been pushed to the DB, 
    # and has been automatically assigned a unique primary key `id`
    db.session.flush()

    # create list of connected controllers
    controllers_list = Controller.query.filter(Controller.controller_type.in_((Controller.entry_controller, 
                                                                               Controller.exit_controller))
                                              ).all()

    # looping IP addresses list
    for controller in controllers_list:
        try:
            # ping all local addresses
            response_decoded = urllib.request.urlopen(f'http://{clean_ip_address(controller.controller_host)}:8001/PING', timeout=15).read().decode('utf-8')
            response_json = json.loads(response_decoded)
            # check if response containt `success`=PONG
            if response_json['success']=='PONG':
                # update controller data
                controller.controller_name=f"Controller {response_json['controller_type']}"
                controller.controller_macaddr=response_json['controller_mac']
                controller.controller_type=response_json['controller_type']

                print(f"Already exist controller updated! Name - Controller {response_json['controller_type']}")

        except urllib.error.URLError as err:
            # delete controller if can't connect
            db.session.delete(controller)
            print(f'Exist controller deleted! IP - {controller.controller_host}; Name - {controller.controller_name}')

        except Exception as err:
            # TODO Add logging
            print(err)
            # delete controller if can't connect
            db.session.delete(controller)
        finally:
            db.session.commit()

    # update completed task in DB
    completed_task = Task.query.get(new_task.id)
    completed_task.task_end_datetime = datetime.now()
    completed_task.task_is_complete = True

    db.session.commit()
    db.session.close()
