# SimpleParking

## Plan
1. Reception module
2. Exit module
3. Entry module

***

## Reception module

Web server based on **Python 3.6** (**Flask**, **SQLAlchemy**), **PostgreSQL**, **Celery + Redis**, **RabbitMQ**.

***

### Flask
#### Flask views:
1. **Index**

    Params:

    Methods:

    Description:
2. **Login**

    Params:

    Methods:

    Description:
3. **Tickets-list**

    Params:

    Methods:

    Description:
4. **Tasks-list**

    Params:

    Methods:

    Description:
5. **New-ticket**

    Params:

    Methods:

    Description:
6. **Edit-ticket**

    Params:

    Methods:

    Description:
7. **Delete-ticket**

    Params:

    Methods:

    Description:
8. **Settings**

    Params: *None*

    Methods: *GET*, *POST*

    Description: User see system settings: **ticket_delete_period**, **ticket_valid_period**, **ticket_text** (Detail description in model *Setting* doc-string). User Can change all settings and set new.
9. **Control-page**

    Params: *None*

    Methods: *GET*, *POST*

    Description: Show to user control panel with all gates and controllers. Here user can **Lock/Unlock** gates. 

    **In future**: *User can add new controllers by adding controller IP, MAC address, type and status + choice controller name.*

10. **Logout**

    **Params**: *None*

    **Methods**: *GET*

    **Description**: Logout user, session clearing, redirect to Index page

***

## Exit module

Web server based on **Python 3.6** (**SQLAlchemy**), **SQLite3**, **RabbitMQ**.

Exit module contain:

1. **Exit-server script**;

    Script run simple Ping/Pong server which run on local machine in local network(IP - `192.168.*.*`), port - `8001`, route - `PING`, method - `GET`;
    Server return JSON, contains:
    ``` python
    'success': "PONG",
    'controller_ip': local network machine IP - "192.168.*.*",
    'controller_type': "exit" or "entry",
    'controller_mac': local machine mac address - "b8:27:eb:58:7c:57"
    ```
    Reception scheduled task after reception script run create map of local network machines(get all local IP's) and start pinging `8001` port and route `PING` them all, to find connected active machines and add them in DB. Then every `N` minutes reception repeat this task to keep controller list in actual status.
2. **Gate wait-messages script**;

    Script looping messages in local RabbitMQ queue(login as `guest/guest`, queue name - `gate_queue`) and waiting messages with folowing tasks - ``` 'open', 'lock_open', 'lock_close', 'unlock' ``` . Then get message, parse it and complete sended instruction, at the end - send to queue command to mark message as confirmed(`ACK`).

    Gate wait script runned in separately procces, in this process script run 2 additional threads - ```lock_gate_opened, lock_gate_closed``` threads. If user send command to lock gate opened/closed, script check gate currently status(flags - ```LOCK_GATE_OPENED, LOCK_GATE_CLOSED```), and if possible - change it. 
    
    1. **`LOCK_GATE_OPENED`** - `True` if gate locked **opened**, else - `False`
    1. **`LOCK_GATE_CLOSED`** - `True` if gate locked **closed**, else - `False`

3. **Ticket wait-scaner script**;
4. **Specific messages wait script**;

    Script looping messages in local RabbitMQ queue(login as `guest/guest`, queue name - `specific_gate_queue`) and waiting messages with folowing tasks - ``` 'settings'``` . Then get message, parse it and complete sended instruction (more about instructions in module local readme or in functions doc_strings), at the end - send to queue command to mark message as confirmed(`ACK`).
    
