#!/bin/bash    

# install redis-server
apt install redis-server -y

# install psotgresql
apt install postgresql postgresql-client -y

# install cups
apt install cups -y

# isntall supervisor
apt install supervisor -y
cp deploy_files/celery_supervisor.conf /etc/supervisor/conf.d/
cp  deploy_files/flask_supervisor.conf /etc/supervisor/conf.d/

supervisorctl reread

supervisorctl update

service supervisor restart
