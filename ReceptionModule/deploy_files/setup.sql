CREATE USER simple_parking_admin WITH encrypted password 'veryhardpass';
CREATE DATABASE simple_parking_db OWNER simple_parking_admin ENCODING 'UNICODE';
