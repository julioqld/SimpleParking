from multiprocessing import Process

from EntryClient import gate
from EntryClient import ticket
from server_entry import run_entry_server
from QueueClient import EntryQueueClient



while True:
    try:
        # create entry gate and entry ticket processes
        entry_server_process = Process(target=run_entry_server)
        entry_gate_process = Process(target=gate.wait_messages)
        entry_ticket_process = Process(target=ticket.wait_tickets)
        # run processes
        entry_server_process.start()
        entry_gate_process.start()
        entry_ticket_process.start()
        # wait until end
        entry_server_process.join()
        entry_gate_process.join()
        entry_ticket_process.join()
    except KeyboardInterrupt:
        break
    except BaseException as err:
        print(err)
        continue
        