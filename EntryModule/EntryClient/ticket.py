import time
import random
import json
from threading import Thread

import barcode
import usb.core
import usb.util
from escpos.printer import Usb
import RPi.GPIO as GPIO

from QueueClient import EntryQueueClient
from EntryClient.settings import PRINT_BUTTON_PIN

def connect_printer()->tuple:
    """
    Function read printer params from `entry_printer_config.json`, check it(connect to printer)
     and return connect credentials
    """
    with open('EntryClient/entry_printer_config.json', 'r') as conf_file:

        printer_config_data = json.loads(conf_file.read())['printer']

    vendor_id = printer_config_data['vendor_id']
    product_id = printer_config_data['product_id']

    dev = usb.core.find(idVendor=vendor_id, idProduct=product_id)
    
    if dev:
        # set the active configuration. With no arguments, the first
        # configuration will be the active one
        #dev.set_configuration()
        # get an endpoint instance
        cfg = dev.get_active_configuration()
        intf = cfg[(0,0)]
        # get endpoints from printer
        endpoint_out = intf[0]
        endpoint_in = intf[1]

        print('Printer success connected')

        return vendor_id, product_id, endpoint_out, endpoint_in
    else:
        return False, False, False, False

def print_ticket(ticket_data_string: int)->bool:
    """
    Function print ticket based on `ticket_data_string` and printer credentials
    """
    try:
        # get printer data
        vendor_id, product_id, endpoint_out, endpoint_in = connect_printer()
        # print new ticket
        if vendor_id and product_id and endpoint_out and endpoint_in:
            '''
            Connect to printer and print data
            '''
            """ Seiko Epson Corp. Receipt Printer (EPSON TM-T88III) """
            p = Usb(vendor_id, product_id, out_ep=endpoint_out, in_ep=endpoint_in)
            p.text("Hello World\n")
            #p.barcode('1324354657687', 'CODE128', 64, 2, '', '')
            p.cut()

            print(ticket_data_string)
            EAN = barcode.get_barcode_class('code128')
            ean = EAN(str(ticket_data_string))
            ean.save('code128_barcode')

            return True
        else:
            print('Re-setup entry printer')
            return False
    except Exception as err:
        print(err)
        return False

def open_gate():
    """
    Function open gate after success ticket creation
    """
    instruction = json.dumps({
                    'task': 'open'
                })
    EntryQueueClient().create_message(message_body=instruction, queue_name='gate_queue')

def create_ticket()->bool:
    """
    Function wait `Print ticket` button pressed and create new ticket
    Function get `timestamp` as barcode code and run print function
    """
    # prepare ticket barcode data
    ticket_timestamp = int(time.time())
    ticket_type = 'T'
    ticket_paid_status = 'U'

    ticket_data_string = '_'.join((ticket_type, str(ticket_timestamp), ticket_paid_status))

    print('.'*22+f'\n| Printing driver ticket|\nNumber - {ticket_data_string} \n'+'.'*22)

    # print new ticket
    if print_ticket(ticket_data_string=ticket_data_string):
        # open gate
        open_gate()
    else:
        print('Cant print ticket!')
    
    # wait some time
    time.sleep(1)
    
    return True


def wait_tickets():
    """
    Function check user ticket in db and send message to open gate, or close it
    """

    print('.'*20+'\n| Wait new drivers |\n'+'.'*20)

    # Ignore warning for now
    GPIO.setwarnings(False) 
    # Use physical pin numbering
    GPIO.setmode(GPIO.BOARD) 
    # Set pin `PRINT_BUTTON_PIN` to be an input pin and set initial value to be pulled low (off)
    GPIO.setup(PRINT_BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    while True: # Run forever
        if GPIO.input(PRINT_BUTTON_PIN) == GPIO.HIGH:
            create_ticket()
        time.sleep(1)

