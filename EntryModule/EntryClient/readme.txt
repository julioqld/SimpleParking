GATE

Gate module connected to RabbitMQ queue and wait commands from reception or scanner.
Command types(the same as in ReceptionWebInterface.models in Task model):
    open_task = 'open' - open gates one time manually
    lock_open_task = 'lock_open' - lock open gates manually
    lock_close_task = 'lock_close' - lock close gates manually
    unlock_task = 'unlock' - unlock gates

If gates is locked nobody can open/close them until reception-user unlock it.
When reception-user unlock gates - gates start recieve `open_task` message.

When gates lock/unlock they update gates status in local DB and in reception DB.
___________________________________________________________________________________
TICKET

Ticket module work with tickets. Module wait driver ticket, check it, if it valid - 
update ticket info in local and reception DB. Then create new message to GATE module 
tasks-queue for open gate.
