import subprocess
import re
import json

import usb.core
import usb.util


def check_printer_connect(vendor_id: str, product_id: str)->bool:
    """
    Function check device credentials, connect to it

    :param vendor_id: Device vendor ID
    :param product_id: Device product ID
    """
    dev = usb.core.find(idVendor=int(vendor_id, 16), idProduct=int(product_id, 16))
    print(dev)
    if dev:
        # set the active configuration. With no arguments, the first
        # configuration will be the active one
        #dev.set_configuration()
        # get an endpoint instance
        cfg = dev.get_active_configuration()
        intf = cfg[(0,0)]
        print('\n')
        print(intf)
        return True
    else:
        return False


def check_devices(devices_list: list, printer_name:str)->list:
    """
    Function find printers in device list, using the `re`

    :param devices_list: List of all devices from `lsusb` command
    :param printer_name: Printer name for search
    """
    # precompile device data search regexp
    vendor_product_compile = re.compile('ID (\w+):(\w+)')
    # loop all devices and try to find printer
    for device in devices_list:
        # check  if printer name in devices list
        if printer_name in device.lower():
            print(f'Find new printer - {device}')
            ids = re.findall(vendor_product_compile, device)[0]
            return ids[0], ids[1]
    return False, False


def save_printer_data(printer_name: str='printer'):
    """
    Try to find printers in connected devices by `bDeviceClass`, for printers == 7
    """
    # get list of devices connected by USB port
    all_usb_devices = subprocess.check_output(['lsusb']).decode()
    # split all devices by `\n`
    all_usb_devices = all_usb_devices.split('\n')
    # try to find printer in devices and get printer ID's for connect
    vendor_id, product_id = check_devices(devices_list=all_usb_devices,
                                          printer_name=printer_name)
    # if printer find
    if vendor_id and product_id:
        # check that programm can connect to printer
        if check_printer_connect(vendor_id=vendor_id, product_id=product_id):
            # save printer data to config file
            with open('EntryClient/entry_printer_config.json', 'w') as outfile:
                printer_data = {
                    'printer':
                        {
                            'vendor_id': int(vendor_id, 16),
                            'product_id': int(product_id, 16),
                            'input_endpoint': '0x82',
                            'output_endpoint': '0x01'
                        }
                    }
                json.dump(printer_data, outfile)
            return
        else:
            print('Can`t connect to printer. Try to reconnect it and edit printer name')
    else:
        print('Can`t find any connected printer. Try to reconnect it and edit printer name')

# for deploy
#save_printer_data('custom engineering spa') 
# for test
save_printer_data('print') 
