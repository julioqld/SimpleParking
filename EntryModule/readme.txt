CLIENT_ENTRY

Script run ticket print module and gate open module in 2 processes;

Print ticket button must be connected to 8(GPIO - 14, physical - 08) pin and to 3.3V;
___________________________________________________________________________________
SERVER ENTRY

Basic python http.server which store info about current controller and send it for response.

Get info about controller:
`http://<IP_address>:8001/PING`

Response, JSON:
    {
        'success': 'PONG',
        'controller_ip': <controller IP address, as string>,
        'controller_type': 'entry',
        'controller_mac': <controller MAC address, as string>
    }

