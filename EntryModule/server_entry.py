import json
import subprocess
from datetime import datetime
from uuid import getnode as get_mac
from http.server import HTTPServer, BaseHTTPRequestHandler


# set port
PORT = 8001
# get machine local IP and run on it server
MACHINE_IP = subprocess.check_output(['hostname','-I']).decode().replace(" \n", '')


class EntryServ(BaseHTTPRequestHandler):

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        # route by `PING`
        if self.path == '/PING':
            # set success header response
            self._set_headers()
            # server response prepare
            response = {
                'success': 'PONG',
                'controller_ip': MACHINE_IP,
                'controller_type': 'entry',
                'controller_mac': ':'.join(("%012X" % get_mac())[i:i+2] for i in range(0, 12, 2))
            }
        
            self.wfile.write(json.dumps(response).encode())

def run_entry_server():
    while True:
        try:
            # print start message
            print(f'Server start on `{MACHINE_IP}:{PORT}` port. At {datetime.now()}')
            # run server
            httpd = HTTPServer((MACHINE_IP, PORT), EntryServ)
            httpd.serve_forever()

        except KeyboardInterrupt:
            print(f'Server stoped by user at {datetime.now()}')
            break

        except BaseException as err:
            print(f'Server stoped by ERROR at {datetime.now()}')
            print(f'Error: {err}')
            continue
