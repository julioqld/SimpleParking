"""
Parse messages with additional commands from reception

 - Update settings
"""
import time
import datetime
import random
import json

import sqlalchemy_utils
from sqlalchemy import update
from QueueClient import ExitQueueClient
from ExitClient import Session

from ExitClient.models import Settings, Ticket, Task


def wait_specific_messages():
    """
    Exit module messages cycle
    Get messages with tasks for exit module
    Manage DB, settings, params
    """

    print('`'*41+'\n| Wait specific messages from reception |\n'+'`'*41)
    exit_queue = ExitQueueClient()

    while True:
        # get message_frame and response message
        method_frame, response = exit_queue.get_message(queue_name = 'specific_gate_queue')

        # if queue not contain any message - drop this cycle, and wait next message 
        if not method_frame and not response:
            continue
            
        # format message format to JSON
        response_json = json.loads(response)

        # if task available to gate
        if response_json.get('task') in (Task.settings_update, 
                                         Task.add_task, 
                                         Task.delete_task,
                                         Task.edit_task):
            # create session to DB
            session = Session()
            
            print(f" [Q] Get '{response_json}'")
            # TODO Add parse message(from queue) logic

            # if update settings task
            if response_json.get('task')==Task.settings_update:
                # parse recieved settings
                parsed_recieved_settings = response_json.get('text')
                # get exist settings data and update it
                settings_data = session.query(Settings).first()

                settings_data.ticket_delete_period = parsed_recieved_settings.get('ticket_delete_period')
                settings_data.ticket_valid_period = parsed_recieved_settings.get('ticket_valid_period')
                settings_data.ticket_text = parsed_recieved_settings.get('ticket_text')
                # commit new data
                session.commit()

                print('Update settings')
            
            # if add new ticket task
            elif response_json.get('task')==Task.add_task:
                # parse recieved settings
                parsed_new_ticket = response_json.get('text')

                # get exist settings data and update it
                new_ticket = Ticket(ticket_number=parsed_new_ticket['ticket_number'],
                                    ticket_type=parsed_new_ticket['ticket_type'],
                                    ticket_paid=parsed_new_ticket['ticket_paid'],
                                    ticket_valid_datetime=datetime.datetime.strptime(parsed_new_ticket['ticket_valid_datetime'], '%Y-%m-%dT%H:%M:%S'),
                                    ticket_creation_datetime=datetime.datetime.strptime(parsed_new_ticket['ticket_creation_datetime'], '%Y-%m-%dT%H:%M:%S.%f'))
                # add new ticket in DB
                session.add(new_ticket)
                # commit new data
                session.commit()
                
                print('New ticket created')
                        
            # if delete ticket task
            elif response_json.get('task')==Task.delete_task:
                # parse recieved settings
                parsed_new_ticket = response_json.get('text')

                # get ticket by number and delete it
                session.query(Ticket).filter(Ticket.ticket_number==parsed_new_ticket['ticket_number']).delete()

                # commit data
                session.commit()
                
                print(f"Ticket #{parsed_new_ticket['ticket_number']} deleted")

            # if update ticket task
            elif response_json.get('task')==Task.edit_task:
                # parse recieved settings
                parsed_ticket = response_json.get('text')
                # Parse ticket valid datetime
                ticket_formated_datetime = datetime.datetime.strptime(parsed_ticket['ticket_valid_datetime'], '%Y-%m-%dT%H:%M:%S')
                # get ticket data
                ticket_data = session.query(Ticket).filter(Ticket.ticket_number==parsed_ticket['ticket_number']).first()
                # update ticket data
                ticket_data.ticket_type = sqlalchemy_utils.types.choice.Choice(code=parsed_ticket['ticket_type'], 
                                                                               value=Ticket.dict_types[parsed_ticket['ticket_type']])
                ticket_data.ticket_valid_datetime = ticket_formated_datetime
                
                # commit data
                session.commit()
                
                print(f"Ticket #{parsed_ticket['ticket_number']} updated")

            # set message as delivered
            exit_queue.success_deliver(method_frame)

            # close session
            session.close()

        time.sleep(1)
