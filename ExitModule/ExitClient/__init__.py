from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
engine = create_engine('sqlite:///ExitClient/exit_client.db', echo=True)

Session = sessionmaker(bind=engine)

# on first run
def prepare_db():
    from ExitClient.models import Settings
    
    session = Session()
    
    # add basic setting if not exist
    if not session.query(Settings).first():
        session.add(Settings(ticket_delete_period=7,
                             ticket_valid_period=3,
                             ticket_text='Hello in SimpleParking'
                            )
                   )

    session.commit()
    session.close()

prepare_db()

from . import gate, ticket, exit_messages, settings
