"""
Check user ticket.
Update ticket data.

Run:
Open gate
Synchronize ticket data with other DB's
"""
import time
import datetime
import random
import json
from threading import Thread

from QueueClient import ExitQueueClient
from ExitClient import Session, settings

from ExitClient.models import Settings, Ticket


# paid ticket valid period
VALID_PAY_HRS = datetime.timedelta(hours=1)

def scan_ticket()->str:
    """
    Function wait ticket and read barcode

    :return: String with readed ticket data
    """

    with open('/dev/hidraw0', 'rb') as fp:

        barcode = ""

        while True:
            # Get the character from the HID
            buffer = fp.read(8)
            if buffer:
                for element in buffer:
                    if element > 0 and element!=40:
                        # If it is not a '2' then it is not the shift key
                        if element != 2:
                            barcode += settings.hid[element]
                            # if barcode is full - return it 
                            if len(barcode)>=13:
                                return barcode
            else:
                # if buffer is empty - return barcode
                return barcode
    #return random.choice(['t-1545600702-u', 'p-1545600702-u', 't-64400315-p', 't-64400315-u'])

def check_ticket_data(ticket_scanned_string: str)->bool:
    """
    Function check ticket data in local DB.

    :param ticket_scanned_string: String contain ticket type, timestamp, paid status, splited by `-`
                                    for example - `t-1545600702-u`

    :return: True - if ticket is valid, False - if ticket datetime expire
    """
    result = False

    try:
        ticket_type, ticket_timestamp, ticket_paid_status = ticket_scanned_string.split('-')
    except ValueError:
        print('Invalid barcode format readed - ', ticket_scanned_string)
        return result

    # create session to DB
    session = Session()
    # get curent datetime 
    current_datetime = datetime.datetime.now()

    # get exist settings ticket_valid_period in hours and create timedelta
    ticket_valid_period = datetime.timedelta(hours=session.query(Settings).first().ticket_valid_period)

    try:
        # if ticket is temporary
        if ticket_type.upper() == 'T':
            # ticket valid datetime > current_datetime
            if current_datetime < datetime.datetime.fromtimestamp(int(ticket_timestamp)) + ticket_valid_period:
                result = True
            # if ticket datetime is expire
            else:
                # if ticket is paid
                if ticket_paid_status.upper() == 'P':
                    # if ticket paid and ticket_datetime*day > current datetime
                    if current_datetime < datetime.datetime.fromtimestamp(int(ticket_timestamp)*datetime.datetime.now().day) + VALID_PAY_HRS:
                        result = True
                    # if paid ticket datetime is expire
                    else:
                        result = False
                # if ticket unpaid
                else:
                    datetime.datetime.now().day
                    result = False
        # if ticket is permanent
        elif ticket_type.upper() == 'P':
            # check ticket in local DB
            ticket = session.query(Ticket).filter_by(ticket_type=Ticket.perm_ticket,
                                                     ticket_creation_datetime=datetime.datetime.fromtimestamp(ticket_timestamp)
                                                    ).first()
            # if ticket exist - open gate, else - close
            if ticket:
                result = True
            else:
                result = False
        else:
            result = False
    except OverflowError as err:
        result = False
    except Exception as err:
        print(err)
        result = False

    # close session
    session.close()

    return result


def wait_tickets():
    """
    Function check user ticket valid period and send message to open gate, or close it
    """

    print('.'*23+'\n| Wait driver tickets |\n'+'.'*23)

    while True:
        ticket_data = scan_ticket()


        # if ticket is  valid - send message to gate and open it
        if ticket_data != settings.MASTER_CODE:
            # Check ticket logic
            if check_ticket_data(ticket_scanned_string=ticket_data):
                # if ticekt valid - send `open gate` message
                instruction = json.dumps({
                    'task': 'open'
                })
                ExitQueueClient().create_message(message_body=instruction, queue_name='gate_queue')

            time.sleep(3)
        else:
            # if ticekt code is MASTER_CODE - send `open gate` message
            instruction = json.dumps({
                'task': 'open'
            })
            ExitQueueClient().create_message(message_body=instruction, queue_name='gate_queue')

            time.sleep(3)
        
        time.sleep(1)
