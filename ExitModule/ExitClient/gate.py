"""
Open/Close gate client
"""
import time
import random
import json
from threading import Thread

import RPi.GPIO as GPIO

from QueueClient import ExitQueueClient
from ExitClient.settings import GATE_PIN


# flags for gate locking operations
LOCK_GATE_OPENED = False
LOCK_GATE_CLOSED = False


class Gate:
    def __init__(self):
        """
        Class for open/close gates
        """
        GPIO.setwarnings(False) 
        # Use physical pin numbering
        GPIO.setmode(GPIO.BOARD) 
        # Set pin `GATE_PIN` as out
        GPIO.setup(GATE_PIN, GPIO.OUT)

    def open(self):
        # send open signal
        GPIO.output(GATE_PIN, True)

    def close(self):
        # send close signal
        GPIO.output(GATE_PIN, False)

    def __del__(self):
        GPIO.cleanup()

    

def open_gate_func(gate_api: Gate):
    """
    Function open gate 1 time and close it
    """
    # check if gates not locked
    if not LOCK_GATE_CLOSED and not LOCK_GATE_OPENED:
        # TODO add wait logic
        print('~'*21 +'\n|  Open gate 1 time  |\n')
        """
        OPEN AND CLOSE GATE LOGIC
        """
        # open gate
        gate_api.open()
        # wait logc
        # ................................
        time.sleep(1)
        # close gate
        gate_api.close()
        print('\n|  Close gate 1 time |\n'+'~'*21)
    else:
        print('*'*31 +'\n| Gates LOCKED, can`t open it |\n'+'*'*31)

def unlock_gate_func(gate_api: Gate):
    """
    Function open gate 1 time and close it
    """
    global LOCK_GATE_CLOSED, LOCK_GATE_OPENED
    # unlock gate
    LOCK_GATE_OPENED = False
    LOCK_GATE_CLOSED = False

    # close gate
    gate_api.close()

    print('-'*19 +'\n| Gates UN-LOCKED |\n'+'-'*19)
    
def lock_gate_opened_func(gate_api: Gate):
    """
    Function lock gate OPENED long time, while reception-user not unlock it 
    """
    print('~'*28 +'\n| Lock gate opened STARTED |\n'+'~'*28)

    while True:
        if LOCK_GATE_OPENED:
            print('~'*34 +'\n|[O] Lock gate opened IN PROCESS |\n')

        while LOCK_GATE_OPENED:
            """
            OPEN GATE LOGIC
            """
            print(' [O] Gate lock opened')
            
            # open gate
            gate_api.open()
            time.sleep(1)

        time.sleep(1)

    print('\n| Lock gate opened STOPED |\n'+'~'*27)

def lock_gate_closed_func(gate_api: Gate):
    """
    Function lock gate CLOSED long time, while reception-user not unlock it 
    """
    print('~'*28 +'\n| Lock gate closed STARTED |\n'+'~'*28)

    while True:
        if LOCK_GATE_CLOSED:  
            print('~'*34 +'\n|[C] Lock gate closed IN PROCESS |\n')

        while LOCK_GATE_CLOSED:
            """
            CLOSE GATE LOGIC
            """
            print(' [C] Gate lock closed')

            # close gate
            gate_api.close()
            time.sleep(1)

        time.sleep(1)

    print('\n| Lock gate closed STOPED |\n'+'~'*27)


def wait_messages():
    """
    Main logic cycle
    Get messages
    Manage gates
    """
    global LOCK_GATE_OPENED
    global LOCK_GATE_CLOSED

    # create gate api
    gate_api = Gate()

    print('~'*33+'\n| Wait messages from exit queue |\n'+'~'*33)
    exit_queue = ExitQueueClient()

    # lock gate threads
    lock_gate_opened = Thread(target=lock_gate_opened_func, name='Lock_Gate_Opened', args=(gate_api, ))
    lock_gate_closed = Thread(target=lock_gate_closed_func, name='Lock_Gate_Closed', args=(gate_api, ))

    # run lock-gate threads forever
    lock_gate_closed.start()
    lock_gate_opened.start()

    while True:
        # get message_frame and response message
        method_frame, response = exit_queue.get_message(queue_name = 'gate_queue')
        
        # if queue not contain any message - drop this cycle, and wait next message
        if not method_frame and not response:
            continue

        # format response message to JSON
        response_json = json.loads(response)

        # if task available to gate
        if response_json.get('task') in ('open', 'lock_open', 'lock_close', 'unlock'):

            print(f" [Q] Get '{response_json}'")
            # TODO Add parse message(from queue) logic

            # if open gate task
            if response_json.get('task')=='open':
                open_gate_func(gate_api)

            # if lock gate opened
            elif response_json.get('task')=='lock_open':
                LOCK_GATE_OPENED = True
                LOCK_GATE_CLOSED = False

            # if lock gate closed
            elif response_json.get('task')=='lock_close':
                LOCK_GATE_OPENED = False
                LOCK_GATE_CLOSED = True

            # if gates must unlock
            elif response_json.get('task')=='unlock':
                unlock_gate_func(gate_api)
            
            # set message as delivered
            exit_queue.success_deliver(method_frame)

            """
            Choice gate action and start it

            If gates UN_LOCKED:
             - Reception-user can open it maually
             - Driver can open it by ticket
             - Gates close automaticaly
            
            If gates LOCKED:
             - Reception-user can lock gates in open/close status
             - Reception-user can unlock gates
            """

        time.sleep(0.5)
