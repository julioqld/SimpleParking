GATE

Gate module connected to RabbitMQ queue and wait commands from reception or scanner.
Command types(the same as in ReceptionWebInterface.models in Task model):
    open_task = 'open' - open gates one time manually
    lock_open_task = 'lock_open' - lock open gates manually
    lock_close_task = 'lock_close' - lock close gates manually
    unlock_task = 'unlock' - unlock gates

If gates is locked nobody can open/close them until reception-user unlock it.
When reception-user unlock gates - gates start recieve `open_task` message.

___________________________________________________________________________________
TICKET

Ticket module work with tickets. Module wait driver ticket, check it, if it valid - create new message to GATE module 
tasks-queue for open gate.

Ticket-string from barcode reader have format:
`p-1545600702-u`
1 element - Ticket type. `t`-temporary(check ticket valid time), `p`-permanent(not check ticket valid date time - just open gate)
2 element - Ticket creation timestamp. Script try check timestamp using local `valid_ticket_time` from settings. If ticket is expire - not open gate, else - open gate.
3 element - Ticket paid status. `u` - unpaid, `p`-paid. If ticket timestamp is expire but ticket is Paid - try `timestamp*current_month_day+paid_ticket_valid_period` and check expire.
