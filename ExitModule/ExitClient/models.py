from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy_utils.types.choice import ChoiceType

from ExitClient import Base, engine

class Settings(Base):
    """
    System settings

    ticket_delete_period - Ticket delete period, in days 
                           (after this period `Temporary` and `ticket_available_to_delete` ticket will be deleted)
    ticket_valid_period - Ticket valid period, in hours
    ticket_text - Text on paper ticket
    """
    __tablename__ = 'settings'

    id = Column(Integer, primary_key=True)
    ticket_delete_period = Column(Integer, default=7)
    ticket_valid_period = Column(Integer, default=3)
    ticket_text = Column(String, default='Simple parking')

    def __init__(self, ticket_delete_period, ticket_valid_period, ticket_text):
        self.ticket_delete_period = ticket_delete_period
        self.ticket_valid_period = ticket_valid_period
        self.ticket_text = ticket_text

    def __repr__(self):
        return f"<Settings (ticket_delete_period={self.ticket_delete_period}, \
                            ticket_valid_period={self.ticket_valid_period}, \
                            ticket_text={self.ticket_text} \
                           ) >"


class Ticket(Base):
    """
    Ticket model

    id - Ticket global ID
    ticket_number - Ticket number placed on paper
    ticket_type - Ticket type - `temporary` or `permanent`
    ticket_paid - Ticket paid - `paid` or `unpaid`
    ticket_creation_datetime - Ticket creation datetime
    ticket_valid_datetime - Ticket validation end datetime
    ticket_scanned_datetime - Datetime when ticket is scanned
    ticket_available_to_delete - If tciket is scanned and it is `Temporary` it is available to delete
    visible - Ticket visibility for user(when delete - change to False)
    """
    __tablename__ = 'ticket'
    dict_types = {
                    'temp':'Temporary',
                    'perm':'Permanent',                    
                 }
    temp_ticket = 'temp'
    perm_ticket = 'perm'
    TYPES = [
        (temp_ticket, 'Temporary'),
        (perm_ticket, 'Permanent')
    ]

    unpaid_ticket = 'unpaid'
    paid_ticket = 'paid' 
    PAID = [
        (unpaid_ticket, 'Unpaid'),
        (paid_ticket, 'Paid')
    ]

    id = Column(Integer, primary_key=True)
    ticket_number = Column(Integer, nullable=False, unique=True)
    ticket_type = Column(ChoiceType(TYPES, impl=String()), default=temp_ticket)
    ticket_paid = Column(ChoiceType(PAID, impl=String()), default=unpaid_ticket)
    ticket_creation_datetime = Column(DateTime(timezone=False), default = datetime.now)
    ticket_valid_datetime = Column(DateTime(timezone=False))
    ticket_scanned_datetime = Column(DateTime(timezone=False), nullable=True)
    ticket_available_to_delete = Column(Boolean, default=False)
    visible = Column(Boolean, default=True)
        
    def __init__(self, ticket_number, ticket_type, ticket_paid, ticket_valid_datetime, ticket_creation_datetime = None):
        self.ticket_number = ticket_number
        self.ticket_type = ticket_type
        self.ticket_paid = ticket_paid
        self.ticket_valid_datetime = ticket_valid_datetime
        self.ticket_creation_datetime = ticket_creation_datetime if ticket_creation_datetime else datetime.now()

    def creation_timestamp(self):
        """
        Method return ticket creation datetime in timestamp format
        """
        return self.ticket_creation_datetime.strftime("%s")

    def create_barcode(self):
        """
        Method return ticket barcode sctring
        """
        return f"""
                {'T' if self.ticket_type.code==self.temp_ticket else 'P'}
                _
                {self.creation_timestamp()}
                _
                {'U' if self.ticket_paid.code==self.unpaid_ticket else 'P'}
                """
        

    def __repr__(self):
        return f"<Settings (id={self.id}, \
                            ticket_number={self.ticket_number}, \
                            ticket_type={self.ticket_type}, \
                            ticket_paid={self.ticket_paid}, \
                            creation_datetime={self.ticket_creation_datetime}, \
                            valid_datetime={self.ticket_valid_datetime} \
                           ) >"
class Task(Base):
    """
    Task model

    id - Task unique ID
    task_type - Task type
    task_name - Task name
    task_description - Task description
    task_is_complete - Task is cimpleted
    """

    __tablename__ = 'task'
    
    # Automatic actions
    synchro_task = 'synch'
    update_task = 'update'
    # User manually ticket actions
    print_task = 'print'
    add_task = 'add'
    delete_task = 'delete'
    edit_task = 'edit'
    # Gate actions
    open_task = 'open'
    close_task = 'close'
    lock_open_task = 'lock_open'
    lock_close_task = 'lock_close'
    unlock_task = 'unlock'
    # Other actions
    settings_update = 'settings'
    controllers_find = 'controllers_find'
    controllers_check = 'controllers_check'

    TYPES = [
        (synchro_task, 'Synchronize DB'),
        (update_task, 'Update ticket status'),

        (print_task, 'Print ticket'),
        (add_task, 'Add new ticket'),
        (delete_task, 'Delete ticket'),
        (edit_task, 'Edit ticket'),

        (open_task, 'Open'),
        (close_task, 'Close'),
        (lock_open_task, 'Lock open'),
        (lock_close_task, 'Lock close'),
        (unlock_task, 'Unlock'),

        (settings_update, 'Update settings'),
        (controllers_find, 'New controllers find'),
        (controllers_check, 'Exist controllers check'),
    ]

    dict_types = {
                    synchro_task:'Synchronize DB',
                    update_task:'Update ticket status',
                    
                    print_task: 'Print ticket',
                    add_task:'Add new ticket',
                    delete_task:'Delete ticket',  
                    edit_task:'Edit ticket',
                    
                    lock_open_task:'Lock open',
                    lock_close_task:'Lock close',
                    unlock_task:'Unlock',
                    open_task: 'Open',
                    close_task: 'Close',

                    settings_update: 'Update settings',
                    controllers_find: 'New controllers find',
                    controllers_check: 'Exist controllers check',
                 }

    id = Column(Integer, primary_key=True)
    task_type = Column(ChoiceType(TYPES, impl=String()))
    task_name = Column(String(100), index=True)
    task_description = Column(String(500))
    task_start_datetime = Column(DateTime(timezone=False), default = datetime.now)
    task_end_datetime = Column(DateTime(timezone=False))
    task_is_complete = Column(Boolean, default=False)


    def __init__(self, task_name, task_type, task_description):
        self.task_name = task_name
        self.task_type = task_type
        self.task_description = task_description

    def __repr__(self):
        return f"<Task (id={self.id}, \
                        task_name={self.task_name}, \
                        task_description={self.task_description[:20]}, \
                        task_is_complete={self.task_is_complete} \
                       ) >"


Base.metadata.create_all(engine)
