from multiprocessing import Process

from ExitClient import gate
from ExitClient import ticket
from ExitClient import exit_messages
from server_exit import run_exit_server
from QueueClient import ExitQueueClient


while True:
    try:
        # create exit gate and exit ticket processes
        exit_server_process = Process(target=run_exit_server)
        exit_gate_process = Process(target=gate.wait_messages)
        exit_ticket_process = Process(target=ticket.wait_tickets)
        exit_messages_process = Process(target=exit_messages.wait_specific_messages)
        # run processes
        exit_server_process.start()
        exit_gate_process.start()
        exit_ticket_process.start()
        exit_messages_process.start()    
        # wait until end
        exit_server_process.join()
        exit_gate_process.join()
        exit_ticket_process.join()
        exit_messages_process.join()
    except KeyboardInterrupt:
        break
    except BaseException as err:
        print(err)
        continue
        