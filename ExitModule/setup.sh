#!/bin/bash    

# install and setup rabbitmq
apt install rabbitmq-server
rabbitmqctl add_user reception password
rabbitmqctl set_permissions reception ".*" ".*" ".*"


# isntall supervisor and run
apt install supervisor -y
cp deploy_files/exit_module_supervisor.conf /etc/supervisor/conf.d/

supervisorctl reread

supervisorctl update

service supervisor restart
