CLIENT_EXIT

Script run ticket check module and gate open module in 2 processes
___________________________________________________________________________________
SERVER EXIT

Basic python http.server which store info about current controller and send it for response.

Get info about controller:
`http://<IP_address>:8001/PING`

Response, JSON:
    {
        'success': 'PONG',
        'controller_ip': <controller IP address, as string>,
        'controller_type': <'exit' or 'entry'>,
        'controller_mac': <controller MAC address, as uppercase string>
    }

